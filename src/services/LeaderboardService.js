import axios from "axios";
import { urls } from "./../constants/Urls";

export class LeaderboardService {
    GetMonthlyLeaderboards = async () => {
        const response = await axios.get(
            urls.leaderboards.getMonthlyLeaderboards
        );
        if (response.status != 200) {
            console.error(response);
            throw new Error(
                `There is a problem loading the monthly leaderboards`
            );
        }
        return response.data;
    };

    async SaveLeaderboardScore(createLeaderboardScoreRequest) {
        const response = await axios.post(
            urls.leaderboards.saveLeaderboardScore,
            createLeaderboardScoreRequest,
            {
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );
        if(response.status != 200) {
            console.error(response);
            throw new Error("Couldn't save leaderboard score");
        }
        return response.data;
    }
}
