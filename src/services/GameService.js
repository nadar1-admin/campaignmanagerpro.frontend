import axios from "axios";
import { urls } from "./../constants/Urls";

export const GetGameSetup = async (difficulty) => {
    const response = await axios.get(urls.GetGameSetup(difficulty));
    if (response.status != 200) {
        console.error(response);
        throw new Error(`Can't get game setup. ${response.data}`);
    }
    return response.data;
};

export const GetVendors = async () => {
    const response = await axios.get(urls.vendors);
    if (response.status != 200) {
        console.error(response);
        throw new Error(`Can't get game vendors. ${response.data}`);
    }
    return response.data;
};

export const SimulateDay = async (
    gameId,
    userName,
    jobsData,
    day,
    difficulty,
    budget,
    simulationVariationId
) => {
    let userBids = [];

    jobsData.forEach((jobData) => {
        userBids = [
            ...userBids,
            ...jobData.vendor_specifics.map((vendorSpecific) => ({
                GameId: gameId,
                UserName: userName,
                JobId: vendorSpecific.job_id,
                VendorId: vendorSpecific.vendor_id,
                Difficulty: difficulty,
                Age: day + 1 /* Days are 0 indexed */,
                Bid: Number(vendorSpecific.current_bid),
                Budget: budget
            })),
        ];
    });

    const response = await axios.post(
        urls.SimulateDay(simulationVariationId),
        userBids
    );
    if (response.status != 200) {
        console.error(response);
        throw new Error(
            `Sorry, there was an error simulating the day. Please contact an administrator`
        );
    }
    return response.data;
};

/**
 * Returns the user improvement feedback
 * @param {number} gameId
 * @returns {string[]}
 */
export const GetUserImprovementFeedback = async (
    gameId,
    difficulty,
    simulationVariationId,
    budget
) => {
    const response = await axios.post(urls.userFeedback, {
        GameId: gameId,
        Difficulty: difficulty,
        SimulationVariationId: simulationVariationId,
        Budget: budget,
    });
    if (response.status != 200) {
        console.error(response);
        throw new Error(`There was an error getting your improvement feedback`);
    }
    return response.data;
};
