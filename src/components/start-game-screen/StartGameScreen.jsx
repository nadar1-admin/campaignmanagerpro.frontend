import React from "react";
import { Link, useHistory } from "react-router-dom";
import { routes } from "../../constants/Routes";
import { PlayClickSoundEffect } from "./../../utils/SoundEffectUtil";
import GameButton from "./../generic/game-button/GameButton";
import GameText from "../generic/game-text/GameText";

import "./StartGameScreen.scss";

const StartGameScreen = () => {
    const history = useHistory();

    return (
        <main className="start-game-screen" style={{ paddingTop: "15vh" }}>
            <div className="start-game-screen__center">
                <GameText
                    text="CAMPAIGN MANAGER"
                    fontSize={35}
                    lineHeight="51px"
                    className="start-game-screen__center__game-title"
                />

                <GameText
                    text="PRO 2021"
                    fontSize={25}
                    lineHeight="51px"
                    className="start-game-screen__center__game-year"
                />

                <GameButton
                    color="primary"
                    text="START"
                    className="start-game-screen__center__btn"
                    onClick={() => {
                        history.push(routes.chooseLevel);
                        document.querySelector("#bg-audio").play();
                    }}
                />
                <GameButton
                    color="secondary"
                    text="LEADER BOARDS"
                    className="start-game-screen__center__btn"
                    onClick={() => {
                        history.push(routes.leaderBoards);
                    }}
                />
            </div>
        </main>
    );
};

export default StartGameScreen;
