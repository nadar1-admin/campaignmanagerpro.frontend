import React from "react";
import {
    PlayClickSoundEffect,
    PlayHoverSoundEffect,
} from "../../../utils/SoundEffectUtil";
import "./GameButton.scss";

const GameButton = (props) => {
    const {
        color /* primary/secondary */,
        text /* optional */,
        pointer = true /* optional */,
        className /* optional */,
        style /* optional */,
        onClick /* optional */,
        children /* optioanl */,
        clickSoundEffect = true /* optional */,
        hoverSoundEffect = true /* optional */,
        hoverAnimation = true /* optional */,
    } = props;

    const _onClick = (e) => {
        if (clickSoundEffect) {
            PlayClickSoundEffect();
        }

        if (onClick) {
            onClick(e);
        }
    };

    let _styles = style || {};
    return (
        <button
            style={{ ..._styles, cursor: pointer ? "pointer" : "default" }}
            onMouseOver={() => {
                if (hoverSoundEffect) {
                    PlayHoverSoundEffect();
                }
            }}
            onClick={_onClick}
            className={`game-button ${color} ${className} ${
                hoverAnimation ? "animate-hover" : ""
            }`}>
            {text && text}
            {children && children}
        </button>
    );
};

export default GameButton;
