import React from "react";
import "./GameText.scss";

const GameText = (props) => {
    const {
        text /* Required */,
        fontSize /* Required */,
        lineHeight /* Optional */,
        className /* Optional */,
    } = props;

    const buildStyles = () => {
        let styles = {
            fontSize,
        };
        if (lineHeight) {
            styles = {
                ...styles,
                lineHeight,
            };
        }

        return styles;
    };

    return (
        <span
            className={`game-text ${className ? className : ""}`}
            style={{ ...buildStyles() }}>
            {text}
        </span>
    );
};

export default GameText;
