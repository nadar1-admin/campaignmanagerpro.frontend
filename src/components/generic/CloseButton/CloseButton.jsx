import React from "react";
import { IoMdClose } from "react-icons/io";
import "./CloseButton.scss";

const CloseButton = (props) => {
    const {
        handleClose /* Required */,
        color /* primary / white */,
        className /* optional */,
    } = props;

    return (
        <button
            className={`close-button ${color} ${className}`}
            onClick={handleClose}>
            <div className="close-button__close">
                <IoMdClose />
            </div>
        </button>
    );
};

export default CloseButton;
