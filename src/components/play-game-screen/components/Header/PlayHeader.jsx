import React, { useContext } from "react";
import LeftStatsGrid from "./LeftStats/LeftStatsGrid";
import Profile from "./Profile/Profile";
import { IoSettingsSharp } from "react-icons/io5";

import { RootStoreContext } from "./../../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";

import "./Header.scss";
import { ParseLevelName } from './../../../../utils/GameHelpers';

const PlayHeader = () => {
    const { gameStore } = useContext(RootStoreContext);

    return (
        <header className="header header-play">
            {gameStore.gameState && (
                <>
                    <Profile 
                    userName={gameStore.userName}
                    level={ParseLevelName(gameStore.gameDifficulty)}
                    />

                    <LeftStatsGrid />

                    <IoSettingsSharp
                        onClick={() => {
                            gameStore.settingsIsOpen = true;
                        }}
                        className="header__setings"
                    />
                </>
            )}
        </header>
    );
};

export default observer(PlayHeader);
