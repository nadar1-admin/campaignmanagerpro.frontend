import React, { useState, useEffect, useContext } from "react";
import LeftStats from "./LeftStats";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "./../../../../../stores/RootStoreContext";

const LeftStatsGrid = () => {
    const { gameStore } = useContext(RootStoreContext);

    return (
        <div className="header__bars">
            <LeftStats
                text="MONEY LEFT"
                value={gameStore?.GetBudgetRemaining ?? 0}
                maxValue={gameStore?.gameState?.given_budget ?? 0}
                width={500}
            />

            <LeftStats
                text="DAYS LEFT"
                value={gameStore?.daysRemaining ?? 0}
                maxValue={gameStore?.gameState?.days_duration ?? 0}
                width={500}
            />

            <LeftStats
                text="ROUND TIME"
                value={gameStore?.roundTimeLeft ?? 0}
                maxValue={gameStore.ROUND_TIME}
                className={gameStore.roundTimeLeft < 20 ? "red" : ""}
                width={500}
            />
        </div>
    );
};

export default observer(LeftStatsGrid);
