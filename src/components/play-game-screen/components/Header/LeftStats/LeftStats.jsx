import React from "react";
import { observer } from "mobx-react-lite";
import ProgressBar from "./../ProgressBar/ProgressBar";
import { CommaNumber } from "../../../../../utils/NumbersHelper";

import './LeftStats.scss';

const LeftStats = (props) => {
    const { text, value, maxValue, width, className } = props;
    return (
        <div className={`header__bars__bar ${className}`} style={{ width }}>
            <span className="header__bars__bar__title">{text}</span>
            <div className="header__bars__bar__progress">
                <ProgressBar value={(value / maxValue) * 100} className={className}/>
            </div>

            <span className="header__bars__bar__value">
                {CommaNumber(value)}/{CommaNumber(maxValue)}
            </span>
        </div>
    );
};

export default observer(LeftStats);
