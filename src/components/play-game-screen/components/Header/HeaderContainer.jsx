import React, { useContext } from "react";
import { useLocation } from "react-router-dom";
import LeftStatsGrid from "./LeftStats/LeftStatsGrid";
import Profile from "./Profile/Profile";
import { IoSettingsSharp } from "react-icons/io5";

import { RootStoreContext } from "../../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";
import PlayHeader from "./PlayHeader";
import Header from "./Header";

import "./Header.scss";
import useIsGameplayScreen from '../../../../hooks/useIsGameplayScreen';

const HeaderContainer = () => {
    const { gameStore } = useContext(RootStoreContext);
    const isPlayScreen = useIsGameplayScreen();

    const RenderHeaderComponent = () => {
        if (isPlayScreen) {
            return <PlayHeader />;
        }

        return <Header />;
    };

    return RenderHeaderComponent();
};

export default observer(HeaderContainer);
