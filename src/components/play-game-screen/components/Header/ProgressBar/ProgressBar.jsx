import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import LinearProgress, {
    LinearProgressProps,
} from "@material-ui/core/LinearProgress";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import "./ProgressBar.scss";
import { observer } from 'mobx-react-lite';

const ProgressBar = (props) => {
    const { value, className } = props;
    // const [progress, setProgress] = React.useState(10);

    // React.useEffect(() => {
    //     const timer = setInterval(() => {
    //         setProgress((prevProgress) =>
    //             prevProgress >= 100 ? 10 : prevProgress + 10
    //         );
    //     }, 800);
    //     return () => {
    //         clearInterval(timer);
    //     };
    // }, []);

    return (
        <Box width="100%">
            <LinearProgress
                className={`game-progress-bar ${className}`}
                variant="determinate"
                value={value}
            />
        </Box>
    );
};

export default observer(ProgressBar);
