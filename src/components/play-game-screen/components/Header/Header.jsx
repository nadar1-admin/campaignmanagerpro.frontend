import React, { useContext } from "react";
import { IoSettingsSharp } from "react-icons/io5";
import { RootStoreContext } from "./../../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";

import "./Header.scss";

import pandoLogicImg from "../../../../assets/images/pandologic-logo-mini.svg";

const Header = () => {
    const { gameStore } = useContext(RootStoreContext);

    return (
        <header className="header header-default">
            <img
                className="header__pandologo"
                src={pandoLogicImg}
                alt="PandoLogic logo"
            />

            <IoSettingsSharp
                onClick={() => {
                    gameStore.settingsIsOpen = true;
                }}
                className="header__setings"
            />
        </header>
    );
};

export default observer(Header);
