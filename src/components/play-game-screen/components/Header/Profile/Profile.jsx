import React from "react";

import easyLogo from "../../../../../assets/images/easy-head-logo.svg";
import "./Profile.scss";
import { observer } from "mobx-react-lite";

const Profile = (props) => {
    const { userName, level } = props;

    return (
        <div className="header__profile">
            <img
                className="header__profile__logo"
                src={easyLogo}
                alt="profile logo"
            />
            <div className="header__profile__info">
                <span className="header__profile__info__name">{userName}</span>
                <span className="header__profile__info__level">{level}</span>
            </div>
        </div>
    );
};

export default observer(Profile);
