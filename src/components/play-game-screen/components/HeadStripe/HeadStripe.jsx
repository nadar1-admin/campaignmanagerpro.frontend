import React, {useContext} from 'react';

import './HeadStripe.scss';
import { RootStoreContext } from './../../../../stores/RootStoreContext';

/**
 * <<props>>
 * title: string
 */
const HeadStripe = (props) => {
    const { gameStore } = useContext(RootStoreContext);
    const {title} = props;

    return (
        <div className="head-stripe">
            <span className="head-stripe__campaign-name">{gameStore.gameState?.campaign_name ?? ""}</span>
            {/* <div className="head-stripe__days-left">
                <span className="head-stripe__days-left__days">
                    14
                </span>
                <span className="head-stripe__days-left__text">
                    days<br />left
                </span>
            </div> */}
        </div>
    )
}

export default HeadStripe
