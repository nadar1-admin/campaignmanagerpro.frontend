import React, { useState, useEffect, useContext } from "react";
import { Drawer } from "antd";
import GameButton from "../../../generic/game-button/GameButton";
import VendorList from "./vendor-list/VendorList";
import { RootStoreContext } from "./../../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";
import { useSnackbar } from "notistack";

import "./VendorSelectionDrawer.scss";

const VendorSelectionDrawer = (props) => {
    const { isOpen, handleClose } = props;
    const { gameStore } = useContext(RootStoreContext);
    const [vendorOptions, setVendorOptions] = useState(
        gameStore.vendorOptions.gameVendorSelectionOptions
    );

    const { enqueueSnackbar } = useSnackbar();

    const HandleBidChange = (bid, vendor) => {
        setVendorOptions({
            ...vendorOptions,
            [vendor.vendor_id]: {
                ...vendorOptions[vendor.vendor_id],
                bid: bid,
            },
        });
    };

    const HandleIsCutOff = (vendor, isCutoff) => {
        setVendorOptions({
            ...vendorOptions,
            [vendor.vendor_id]: {
                ...vendorOptions[vendor.vendor_id],
                isCutoff: isCutoff,
            },
        });
    };

    const HandleApplySelection = () => {
        for (let key in vendorOptions) {
            const vendorData = vendorOptions[key];

            if (vendorData.isCutoff) {
                gameStore.bidHandler.SetBidForAllJobsWithVendor(key, 0);
                continue;
            }

            gameStore.bidHandler.SetBidForAllJobsWithVendor(
                key,
                vendorData.bid
            );
        }

        enqueueSnackbar("Vendor selection applied");
        gameStore.vendorOptions.SetGameVendorSelectionOptions(vendorOptions);
        handleClose();
    };

    return (
        <Drawer
            width={650}
            className="vendor-selection-drawer"
            title="Vendor Selection"
            placement="right"
            closable={true}
            onClose={handleClose}
            visible={isOpen}
            key="right">
            <div className="vendor-selection-drawer-wrapper">
                <VendorList
                    vendorOptions={vendorOptions}
                    HandleBidChange={HandleBidChange}
                    HandleIsCutOff={HandleIsCutOff}
                />

                <GameButton
                    onClick={HandleApplySelection}
                    color="primary"
                    text="Apply Selections"
                    className="vendor-selection-drawer-wrapper__apply-btn"
                />
            </div>
        </Drawer>
    );
};

export default observer(VendorSelectionDrawer);
