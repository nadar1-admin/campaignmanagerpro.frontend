import React, { useState, useEffect, useContext } from "react";
import { RootStoreContext } from "./../../../../../stores/RootStoreContext";
import { Box, TextField, ButtonGroup, Button } from "@material-ui/core";
import SquareLoader from "./../../../../generic/loader/Loader";
import { observer } from "mobx-react-lite";

const VendorComponent = (props) => {
    const { vendor, bid, isCutoff, HandleBidChange, HandleIsCutOff } =
        props;
        const [isIncluded, setIsIncluded] = useState(!isCutoff);

    return (
        <div
            className="vendor-list__component"
            style={{
                backgroundColor: isIncluded
                    ? "inherit"
                    : "rgba(229, 57, 53, 0.15)",
            }}>
            <div className="vendor-list__component__vendor-data">
                <div className="vendor-list__component__vendor-data__circle">
                    <img
                        className="vendor-list__component__vendor-data__circle__img"
                        src={vendor.logoUrl}
                        alt="vendor-logo"
                    />
                </div>

                <span className="vendor-list__component__vendor-data__title">
                    {vendor.vendor_name}
                </span>
            </div>

            <div className="vendor-list__component__vendor-bid">
                <TextField
                    defaultValue={bid}
                    onChange={(event) => {
                        HandleBidChange(event.target.value, vendor);
                    }}
                    type="number"
                    className="vendor-list__component__vendor-bid__input"
                    label="Bid"
                />
            </div>

            <div className="vendor-list__component__vendor-actions">
                <ButtonGroup
                    color="primary"
                    aria-label="outlined primary button group">
                    <Button
                        onClick={() => {
                            setIsIncluded(true);
                            HandleIsCutOff(vendor, false);
                        }}
                        variant={isIncluded ? "contained" : "outlined"}>
                        Include
                    </Button>
                    <Button
                        onClick={() => {
                            setIsIncluded(false);
                            HandleIsCutOff(vendor, true);
                        }}
                        variant={!isIncluded ? "contained" : "outlined"}>
                        Exclude
                    </Button>
                </ButtonGroup>
            </div>
        </div>
    );
};

export default observer(VendorComponent);
