import React, { useState, useEffect, useContext } from "react";
import { RootStoreContext } from "./../../../../../stores/RootStoreContext";
import { Box, TextField, ButtonGroup, Button } from "@material-ui/core";
import SquareLoader from "./../../../../generic/loader/Loader";
import { observer } from "mobx-react-lite";
import VendorComponent from "./VendorComponent";
import "./VendorList.scss";

const VendorList = (props) => {
    const { HandleBidChange, HandleIsCutOff, vendorOptions } = props;
    const { gameStore } = useContext(RootStoreContext);
    const [vendorList, setVendorList] = useState();

    useEffect(() => {
        (async () => {
            setVendorList(await gameStore.vendorOptions.GetVendorList());
        })();
    }, []);

    if (!vendorList) {
        return (
            <Box>
                <SquareLoader />
            </Box>
        );
    }
    return (
        <div className="vendor-list">
            {vendorList.map((vendor) => (
                <VendorComponent
                    key={vendor.vendor_id}
                    vendor={vendor}
                    bid={vendorOptions[vendor.vendor_id]?.bid ?? undefined}
                    isCutoff={
                        vendorOptions[vendor.vendor_id]?.isCutoff ?? false
                    }
                    HandleBidChange={HandleBidChange}
                    HandleIsCutOff={HandleIsCutOff}
                />
            ))}
        </div>
    );
};

export default observer(VendorList);
