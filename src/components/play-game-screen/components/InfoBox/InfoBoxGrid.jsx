import React, {useContext} from "react";
import { RootStoreContext } from "../../../../stores/RootStoreContext";
import InfoBox from "./InfoBox";

import './InfoBoxGrid.scss';
import { observer } from 'mobx-react-lite';

const InfoBoxGrid = () => {
    const { gameStore } = useContext(RootStoreContext);
    
    return (
        <div className="info-box-grid">
            <InfoBox
                title="Jobs"
                data={[
                    {
                        subTitle: "Total",
                        value: gameStore.gamePerformance.UserAggregatedPerformance.jobsCount,
                    }
                ]}
            />
            <InfoBox
                title="Performance"
                data={[
                    {
                        subTitle: "Clicks",
                        value: gameStore.gamePerformance.UserAggregatedPerformance.clicks,
                    },
                    {
                        subTitle: "Applicants",
                        value: gameStore.gamePerformance.UserAggregatedPerformance.applicants,
                    },
                    {
                        subTitle: "Cost",
                        value: `$${gameStore.gamePerformance.UserAggregatedPerformance.cost}`,
                    },
                    {
                        subTitle: "Conversion Rate",
                        value: `${gameStore.gamePerformance.UserAggregatedPerformance.cvr.toFixed(2)}%`,
                    },
                ]}
            />
            <InfoBox
                title="Your Goals"
                data={[
                    {
                        subTitle: "Applicants",
                        value: gameStore.gameState?.applicants_goal ?? 0,
                    }
                ]}
            />
            <InfoBox
                title="Remaining Resources"
                data={[
                    {
                        subTitle: "Budget Remaining",
                        value: `$${gameStore.GetBudgetRemaining}`,
                    },
                    {
                        subTitle: "Days Remaining",
                        value: gameStore.daysRemaining,
                    }
                ]}
            />
        </div>
    );
};

export default observer(InfoBoxGrid);
