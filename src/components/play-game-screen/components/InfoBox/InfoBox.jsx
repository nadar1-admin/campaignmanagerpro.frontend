import React from "react";

import './InfoBox.scss';
/**
 * <<props>>
 * title: string
 * data: {subTitle: string, value: any}[]
 */
const InfoBox = (props) => {
    const { title, data } = props;

    return (
        <div className="info-box">
            <h3 className="info-box__title">{title}</h3>
            <div className="info-box__content">
                {data.map((dataPoint, index) => (
                    <div key={index} className="info-box__content__item">
                    <p className="info-box__content__item__title">{dataPoint.subTitle}</p>
                    <p className="info-box__content__item__value">{dataPoint.value}</p>
                </div>
                ))}

                {/* <div className="info-box__content__item">
                    <p className="info-box__content__item__title">Clicks</p>
                    <p className="info-box__content__item__value">2,015</p>
                </div>
                <div className="info-box__content__item">
                    <p className="info-box__content__item__title">Applicants</p>
                    <p className="info-box__content__item__value">124</p>
                </div>
                <div className="info-box__content__item">
                    <p className="info-box__content__item__title">Conversion RAte</p>
                    <p className="info-box__content__item__value">6.15%</p>
                </div> */}
            </div>
        </div>
    );
};

export default InfoBox;
