import { AgGridColumn } from "ag-grid-react/lib/agGridColumn";
import { AgGridReact } from "ag-grid-react/lib/agGridReact";
import { toJS } from "mobx";
import { observer } from "mobx-react-lite";
import React, { useContext, useState, useEffect } from "react";
import { RootStoreContext } from "../../../../stores/RootStoreContext";
import { useSnackbar } from "notistack";
import { IsNumber } from "./../../../../utils/NumbersHelper";
import { LicenseManager } from "@ag-grid-enterprise/core";
import GlobalActionButtons from "./global-action-buttons/GlobalActionButtons";
import GlobalBidDialog from "./global-bid-dialog/GlobalBidDialog";

import "ag-grid-enterprise";
import "ag-grid-community";
import "./JobsGrid.scss";
import "./ag-grid-custom.scss";
import VendorSelectionDrawer from "../vendor-selection-drawer/VendorSelectionDrawer";

LicenseManager.setLicenseKey(process.env.REACT_APP_AG_GRID_TOKEN);

const JobsGrid = () => {
    const { gameStore } = useContext(RootStoreContext);
    const [isGlobalBidDialogOpen, setisGlobalBidDialogOpen] = useState(false);
    const [isVendorSelectionDrawerOpen, setIsVendorSelectionDrawerOpen] =
        useState(false);

    const [gridApi, setGridApi] = useState(null);
    const [gridColumnApi, setGridColumnApi] = useState(null);
    const { enqueueSnackbar } = useSnackbar();

    /**
     *  Functions
     */

    const onGridReady = (params) => {
        setGridApi(params.api);
        setGridColumnApi(params.columnApi);
    };

    const onFirstDataRendered = (params) => {
        // setTimeout(function () {
        //     params.api.getDisplayedRowAtIndex(1).setExpanded(true);
        // }, 0);
    };

    const HandleBidCellEdit = (cellType, event) => {
        const newBid = event.newValue;
        if (!IsNumber(newBid)) {
            enqueueSnackbar("Bid must be a valid number", {
                variant: "error",
            });
            return;
        }

        let rowIndex;
        if (cellType == "parent") {
            rowIndex = gameStore.bidHandler.SetBidForAllVendorsInJob(
                event.data.job_id,
                newBid
            );
        } else if (cellType == "child") {
            rowIndex = gameStore.bidHandler.SetBidForVendor(
                event.data.job_id,
                event.data.vendor_id,
                newBid
            );
        }

        window.setTimeout(() => {
            if (rowIndex != -1) {
                gridApi.getDisplayedRowAtIndex(rowIndex).setExpanded(true);
            }
        }, 1);
    };

    const detailCellRendererParams = {
        // provide the Grid Options to use on the Detail Grid
        detailGridOptions: {
            columnDefs: [
                {
                    field: "vendor_name",
                    headerName: "Vendor Name",
                    width: 300 /* checkboxSelection: true */,
                },
                { field: "applicants", headerName: "Applicants", width: 245 },
                { field: "clicks", headerName: "Clicks", width: 245 },
                {
                    field: "cost",
                    headerName: "Cost",
                    width: 245,
                    valueFormatter: (dataModel) =>
                        dataModel.data.cost.toFixed(2),
                },
                {
                    field: "cpa",
                    headerName: "CPA",
                    width: 245,
                    valueFormatter: (dataModel) =>
                        dataModel.data.cpa.toFixed(2),
                },
                {
                    field: "cvr",
                    headerName: "CVR",
                    width: 245,
                    valueFormatter: (dataModel) =>
                        dataModel.data.cvr.toFixed(2),
                },
                {
                    field: "current_bid",
                    headerName: "Current Bid",
                    editable: true,
                    cellEditor: "agPopupTextCellEditor",
                    // onCellValueChanged: HandleDetailCellEdit,
                    valueSetter: (e) => HandleBidCellEdit("child", e),
                },
            ],
        },
        // get the rows for each Detail Grid
        getDetailRowData: (params) => {
            params.successCallback(params.data.vendor_specifics);
        },
    };

    return (
        <div>
            <GlobalActionButtons
                handleGlobalBidClick={() => setisGlobalBidDialogOpen(true)}
                handleVendorSelectionButtonClick={() =>
                    setIsVendorSelectionDrawerOpen(true)
                }
            />

            <GlobalBidDialog
                isOpen={isGlobalBidDialogOpen}
                handleClose={() => setisGlobalBidDialogOpen(false)}
            />

            <VendorSelectionDrawer
                isOpen={isVendorSelectionDrawerOpen}
                handleClose={() => setIsVendorSelectionDrawerOpen(false)}
            />

            <div
                className="ag-theme-alpine jobs-grid"
                style={{ width: "100%", height: "600px" }}>
                <AgGridReact
                    // onCellEditingStopped={HandleMasterCellEdit}
                    defaultColDef={{ flex: 1 }}
                    masterDetail={true}
                    detailCellRendererParams={detailCellRendererParams}
                    onGridReady={onGridReady}
                    onFirstDataRendered={onFirstDataRendered}
                    rowData={gameStore.jobsData}>
                    <AgGridColumn headerName="Details">
                        <AgGridColumn
                            field="title"
                            headerName="Job Title"
                            cellRenderer="agGroupCellRenderer"
                        />
                        <AgGridColumn field="location" headerName="Location" />
                    </AgGridColumn>
                    <AgGridColumn headerName="Performance">
                        <AgGridColumn field="clicks" headerName="Clicks" />
                        <AgGridColumn
                            field="applicants"
                            headerName="Applicants"
                        />
                        <AgGridColumn
                            field="cost"
                            headerName="Cost"
                            valueFormatter={(dataModel) =>
                                dataModel.data.cost.toFixed(2)
                            }
                        />
                        <AgGridColumn
                            field="cpa"
                            headerName="CPA"
                            valueFormatter={(dataModel) =>
                                dataModel.data.cpa.toFixed(2)
                            }
                        />
                        <AgGridColumn
                            field="cvr"
                            headerName="CVR"
                            valueFormatter={(dataModel) =>
                                dataModel.data.cvr.toFixed(2)
                            }
                        />
                    </AgGridColumn>
                    <AgGridColumn headerName="Actions">
                        <AgGridColumn
                            editable
                            cellEditor="agPopupTextCellEditor"
                            field="current_bid"
                            valueSetter={(e) => HandleBidCellEdit("parent", e)}
                            // valueGetter={e => {
                            //     return 3;
                            // }}

                            headerName="Current Bid"
                        />
                    </AgGridColumn>
                </AgGridReact>
            </div>
        </div>
    );
};

export default observer(JobsGrid);
