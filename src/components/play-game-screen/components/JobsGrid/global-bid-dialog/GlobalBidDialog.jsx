import React, { useContext, useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Box } from "@material-ui/core";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "../../../../../stores/RootStoreContext";

import "./GlobalBidDialog.scss";
import { IsNumber } from "../../../../../utils/NumbersHelper";
import { useSnackbar } from "notistack";

const GlobalBidDialog = (props) => {
    const { isOpen, handleClose } = props;
    const [bid, setBid] = useState();
    const { gameStore } = useContext(RootStoreContext);
    const { enqueueSnackbar } = useSnackbar();

    const handleApply = (applyType) => {
        if (!IsNumber(bid)) {
            enqueueSnackbar("Global bid should be a valid number", {
                variant: "error",
            });
            return;
        }
        if (applyType == "all") {
            gameStore.bidHandler.SetBidForAllJobs(Number(bid));
            enqueueSnackbar(`Global bid has set to: $${bid}`);
            handleClose();
            return;
        } else if (applyType == "no-bids-jobs") {
            gameStore.bidHandler.SetBidForJobsAndAllVendorsOfJobWhere(
                Number(bid),
                (jobEntry) => Number(jobEntry.current_bid) == 0
            );
            enqueueSnackbar(`Bid was set to: $${bid} for jobs without bid`);
            handleClose();
            return;
        }

        throw new Error("Unhandled apply type");
    };

    return (
        <div>
            <Dialog
                open={isOpen}
                onClose={handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Global Bid</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Select a bid to be applied for all jobs
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="Bid"
                        placeholder="Example: 1.04"
                        // type="number"
                        fullWidth
                        value={bid}
                        onChange={(event) => {
                            setBid(event.target.value);
                        }}
                    />
                </DialogContent>
                <DialogActions className="global-bid-dialog__buttons-actions">
                    <Box display="flex" flexDirection="column" p={5}>
                        <Button
                            onClick={handleClose}
                            color="primary"
                            variant="text"
                            style={{ marginBottom: 20 }}>
                            Cancel
                        </Button>
                        <Button
                            onClick={() => handleApply("all")}
                            color="primary"
                            variant="contained"
                            style={{ marginBottom: 20 }}>
                            Apply for all
                        </Button>
                        <Button
                            onClick={() => handleApply("no-bids-jobs")}
                            color="primary"
                            variant="contained"
                            style={{ marginBottom: 20 }}>
                            Apply for no bid jobs
                        </Button>
                    </Box>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default observer(GlobalBidDialog);
