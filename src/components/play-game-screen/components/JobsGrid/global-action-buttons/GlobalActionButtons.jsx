import React, { useContext } from "react";
import { Box, Tooltip } from "@material-ui/core";
import {
    GiPerspectiveDiceSixFacesRandom,
    GiChoice,
    GiWaterRecycling,
} from "react-icons/gi";
import { BsReplyAllFill } from "react-icons/bs";

import { RootStoreContext } from "../../../../../stores/RootStoreContext";
import { useSnackbar } from "notistack";

import "./GlobalActionButtons.scss";

const GlobalActionButtons = (props) => {
    const { handleGlobalBidClick, handleVendorSelectionButtonClick } = props;
    const { gameStore } = useContext(RootStoreContext);
    const { enqueueSnackbar } = useSnackbar();

    return (
        <section>
            <div className="global-actions">
                <Tooltip title="Resets all bids to 0" placement="top-start">
                    <div
                        className="global-actions__btn reset"
                        onClick={() => {
                            gameStore.bidHandler.SetBidForAllJobs(0);
                            enqueueSnackbar("All bids have been reset");
                        }}>
                        <GiWaterRecycling />
                        <span>Reset</span>
                    </div>
                </Tooltip>
                <Tooltip
                    title="Generates a random bid and places it in all jobs"
                    placement="top-start">
                    <div
                        className="global-actions__btn random"
                        onClick={() => {
                            gameStore.bidHandler.SetRandomBidsForAll();
                            enqueueSnackbar("Placed random bid");
                        }}>
                        <GiPerspectiveDiceSixFacesRandom />

                        <span>Random Seed</span>
                    </div>
                </Tooltip>
                <div
                    onClick={handleGlobalBidClick}
                    className="global-actions__btn global-bid">
                    <BsReplyAllFill />
                    <span>Global Bid</span>
                </div>
                <div
                    onClick={handleVendorSelectionButtonClick}
                    className="global-actions__btn vendor-actions">
                    <GiChoice />
                    <span>Vendor Selection</span>
                </div>
            </div>
        </section>
    );
};

export default GlobalActionButtons;
