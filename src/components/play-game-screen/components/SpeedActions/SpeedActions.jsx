import React, { useContext } from "react";
import SpeedDial from "@material-ui/lab/SpeedDial";
import SpeedDialIcon from "@material-ui/lab/SpeedDialIcon";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import { GiPerspectiveDiceSixFacesRandom, GiChoice } from "react-icons/gi";
import { BsReplyAllFill } from "react-icons/bs";
import { RootStoreContext } from "./../../../../stores/RootStoreContext";
import { useSnackbar } from "notistack";
import { Popconfirm, message, Button } from 'antd';

import "./SpeedActions.scss";


const SpeedActions = () => {
    const [open, setOpen] = React.useState(false);
    const [hidden, setHidden] = React.useState(false);
    const { gameStore } = useContext(RootStoreContext);
    const { enqueueSnackbar } = useSnackbar();

    const handleVisibility = () => {
        setHidden((prevHidden) => !prevHidden);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const actions = [
        {
            icon: <GiPerspectiveDiceSixFacesRandom />,
            name: "Random Seed",
            class: "random-seed",
            onClick: () => {
                gameStore.bidHandler.SetRandomBidsForAll();
            },
        },
        {
            icon: <BsReplyAllFill />,
            name: "Global Bid",
            class: "global-bid",
            onClick: () => {
            },
        },
        {
            icon: <GiChoice />,
            name: "Vendor Actions",
            class: "vendor-actions",
            onClick: () => {
            },
        },
    ];

    return (
        <div>
            <SpeedDial
                ariaLabel="SpeedDial tooltip example"
                className="speed-actions"
                hidden={hidden}
                icon={<SpeedDialIcon />}
                onClose={handleClose}
                onOpen={handleOpen}
                open={open}>
                {actions.map((action) => (
                        <SpeedDialAction
                            className={action.class}
                            key={action.name}
                            icon={action.icon}
                            tooltipTitle={action.name}
                            tooltipOpen
                            onClick={action.onClick}
                            // onClick={handleClose}
                        />
                       
                ))}
            </SpeedDial>
        </div>
    );
};

export default SpeedActions;
