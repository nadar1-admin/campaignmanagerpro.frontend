import React, { useContext, useState, useEffect } from "react";
import { RootStoreContext } from "../../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";
import { ButtonGroup } from "@material-ui/core";
import { Button } from "@material-ui/core";

const ChartSectionButtons = ({ activeSection }) => {
    const { gameStore } = useContext(RootStoreContext);

    const isButtonActive = (sectionName) => {
        return activeSection == sectionName;
    };

    const HandleButtonClick = (sectionName) => {
        gameStore.gamePerformance.SetChartActiveSection(sectionName);
    };

    return (
        <ButtonGroup variant="contained" color="primary">
            <Button
                onClick={(_) => HandleButtonClick("Clicks")}
                className={`performance-chart-sections__btn clicks ${
                    isButtonActive("Clicks") && "active"
                }`}>
                Clicks
            </Button>
            <Button
                onClick={(_) => HandleButtonClick("Applicants")}
                className={`performance-chart-sections__btn applicants ${
                    isButtonActive("Applicants") && "active"
                }`}>
                Applicants
            </Button>
            <Button
                onClick={(_) => HandleButtonClick("Cost")}
                className={`performance-chart-sections__btn cost ${
                    isButtonActive("Cost") && "active"
                }`}>
                Cost
            </Button>
        </ButtonGroup>
    );
};

export default observer(ChartSectionButtons);
