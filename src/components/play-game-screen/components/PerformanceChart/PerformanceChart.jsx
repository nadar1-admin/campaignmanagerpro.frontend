// https://recharts.org/en-US/examples

import React, { useContext, useState, useEffect } from "react";
import {
    CartesianGrid,
    Legend,
    Line,
    LineChart,
    ResponsiveContainer,
    XAxis,
    YAxis,
    Tooltip,
} from "recharts";
import { RootStoreContext } from "../../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";
import ChartSectionButtons from "./ChartSectionButtons";
import { Box } from "@material-ui/core";

import "./PerformanceChart.scss";

const PerformanceChart = () => {
    const { gameStore } = useContext(RootStoreContext);
    const [activeSection, setActiveSection] = useState(
        gameStore.gamePerformance.ChartActiveSection
    );

    /* Hooks */
    useEffect(() => {
        setActiveSection(gameStore.gamePerformance.ChartActiveSection);
    }, [gameStore.gamePerformance.ChartActiveSection]);

    return (
        <div className="performance-chart-sections">
            <Box display="flex" justifyContent="space-between">
                <ChartSectionButtons activeSection={activeSection} />
            </Box>

            <div className="performance-chart">
                <ResponsiveContainer width={"99%"} height={300}>
                    <LineChart
                        data={gameStore.gamePerformance.ChartPerformance}
                        /* margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }} */
                    >
                        {/* <CartesianGrid strokeDasharray="3 3" /> */}
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />

                        {activeSection == "Applicants" && (
                            <Line
                                type="monotone"
                                dataKey="applicantsGoal"
                                name="Applicants Goal"
                                stroke="#DC143C"
                                activeDot={{ r: 8 }}
                                strokeDasharray="5 5"
                            />
                        )}

                        <Line
                            type="monotone"
                            dataKey={`algo${activeSection}`}
                            name={`Algo ${activeSection}`}
                            stroke="#8884d8"
                            activeDot={{ r: 8 }}
                        />

                        <Line
                            type="monotone"
                            dataKey={`user${activeSection}`}
                            name={`User ${activeSection}`}
                            stroke="#82ca9d"
                        />
                    </LineChart>
                </ResponsiveContainer>
            </div>
        </div>
    );
};

export default observer(PerformanceChart);
