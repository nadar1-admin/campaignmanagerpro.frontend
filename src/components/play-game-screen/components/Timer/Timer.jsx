import React, {useContext} from 'react'
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import { RootStoreContext } from '../../../../stores/RootStoreContext';
import { observer } from "mobx-react-lite";

import "./Timer.scss";

const minuteSeconds = 60;
const hourSeconds = 3600;
const daySeconds = 86400;

const timerProps = {
  isPlaying: true,
  size: 200,
  strokeWidth: 6
};

const renderTime = (dimension, time) => {
  return (
    <div className="time-wrapper">
      <div className="time">{time}</div>
      <div>{dimension}</div>
    </div>
  );
};

const getTimeSeconds = (time) => (minuteSeconds - time) | 0;

const Timer = () => {
  const { gameStore } = useContext(RootStoreContext);

  const stratTime = Date.now() / 1000; // use UNIX timestamp in seconds
  const endTime = stratTime + 243248; // use UNIX timestamp in seconds

  const remainingTime = endTime - stratTime;
  const days = Math.ceil(remainingTime / daySeconds);
  const daysDuration = days * daySeconds;

  return (
    <div className="App">
      <CountdownCircleTimer
        {...timerProps}
        colors={[["#1E8281"]]}
        isPlaying={!gameStore.gameResult.IsGameFinished}
        duration={minuteSeconds}
        initialRemainingTime={gameStore.ROUND_TIME}
        onComplete={(totalElapsedTime) => {
          gameStore.SimulateDay();
          return [
            remainingTime - totalElapsedTime > 0
          ]
        }}
      >
        {({ elapsedTime }) =>
          {
            gameStore.SetRoundTimeLeft(gameStore.ROUND_TIME - elapsedTime);
            return renderTime("NEW DAY BEING IN", getTimeSeconds(elapsedTime))
          }
        }
      </CountdownCircleTimer>
    </div>
  );
}


export default observer(Timer)