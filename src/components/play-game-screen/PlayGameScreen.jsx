import React, { useContext, useEffect } from "react";
import HeadStripe from "./components/HeadStripe/HeadStripe";
import InfoBoxGrid from "./components/InfoBox/InfoBoxGrid";
import PerformanceChart from "./components/PerformanceChart/PerformanceChart";
import { Box, Button, ButtonGroup } from "@material-ui/core";
import HorizontalDivider from "../generic/horizontal-divider/HorizontalDivider";
import JobsGrid from "./components/JobsGrid/JobsGrid";
import ActionButton from "./components/ActionButton/ActionButton";
import { observer } from "mobx-react-lite";
import Timer from "./components/Timer/Timer";
import { RootStoreContext } from "../../stores/RootStoreContext";
import { PlaySimulateActionSoundEffect } from "../../utils/SoundEffectUtil";
import Loader from "../generic/loader/Loader";
import GameResultModal from "../modal/modal-types/GameResultModal";
import SettingsModal from "../modal/modal-types/SettingsModal";
import { urls } from "./../../constants/Urls";
import SpeedActions from "./components/SpeedActions/SpeedActions";

import { Redirect } from "react-router-dom";
import { routes } from "./../../constants/Routes";
import "./PlayGameScreen.scss";

const PlayGameScreen = (props) => {
    const { gameStore } = useContext(RootStoreContext);

    const renderComponentBasedOnState = () => {
        if (
            !gameStore.gameState ||
            !gameStore.jobsData ||
            gameStore.jobsData.length == 0 ||
            gameStore.showLoaderInGameScreen
        ) {
            return (
                <Box mt={10}>
                    <Loader />
                </Box>
            );
        }

        return (
            <>
                <div className="top-section">
                    <div className="top-section__left">
                        <HeadStripe title="Boston Dynamics 21" />

                        <InfoBoxGrid />
                    </div>

                    <div className="top-section__right">
                        <Timer />
                    </div>
                </div>

                <Box mt={8}>
                    <HorizontalDivider text="Performance" />
                    <PerformanceChart />
                </Box>

                <Box mt={8}>
                    <HorizontalDivider text="Jobs Performance" />
                    <JobsGrid />
                </Box>

                <Box mt={8}>
                    <ActionButton
                        onClick={() => PlaySimulateActionSoundEffect()}
                    />
                </Box>

                {/* <SpeedActions /> */}
            </>
        );
    };

    if (!gameStore.userName && gameStore.gameState == undefined) {
        return <Redirect to={routes.chooseLevel} />;
    }

    return (
        <div id="play-game-screen" style={{ paddingTop: 100 }}>
            <GameResultModal />

            {renderComponentBasedOnState()}
        </div>
    );
};

export default observer(PlayGameScreen);
