import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { routes } from "./../../constants/Routes";
import "./LeaderboardsScreen.scss";

import place1 from "../../assets/images/1-place-logo.svg";
import place2 from "../../assets/images/2-place-logo.svg";
import place3 from "../../assets/images/3-place-logo.svg";
import GameButton from "./../generic/game-button/GameButton";
import CloseButton from "../generic/CloseButton/CloseButton";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "./../../stores/RootStoreContext";
import { Box } from "@material-ui/core";
import SquareLoader from "./../generic/loader/Loader";
import { CommaNumber } from "./../../utils/NumbersHelper";
import { Empty } from "antd";
const images = [place1, place2, place3];

const DetermineRank = (rankNumber) => {
    if (rankNumber >= 1 && rankNumber <= 3) {
        return (
            <img
                src={images[rankNumber - 1]}
                alt="placeImg"
                className="place-logo"
            />
        );
    }

    return rankNumber;
};

const DetermineGameLevel = (difficulty) => {
    switch (difficulty) {
        case 1:
            return "Easy";
        case 2:
            return "Medium";
        case 3:
            return "Hard";
        default:
            return "God Level";
    }
};

const LeaderboardsScreen = () => {
    const [leaderboards, setLeaderboards] = useState();
    const { gameStore, leaderboardStore } = useContext(RootStoreContext);
    const history = useHistory();

    const IsCurrentUserScore = (leaderboardScore) => {
        return leaderboardScore.Id == (leaderboardStore?.Rank?.Id ?? -1);
    };

    useEffect(() => {
        (async () => {
            setLeaderboards(await leaderboardStore.GetMonthlyLeaderboards());
        })();
    }, []);

    const RenderComponent = () => {};

    return (
        <main className="leaderboards">
            {!leaderboards ? (
                <Box pt="25px">
                    <SquareLoader />
                </Box>
            ) : (
                <div className="leaderboards__board">
                    {leaderboards.length == 0 ? (
                        <Box pt="25px">
                            <Empty
                                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                                imageStyle={{
                                    height: 60,
                                }}
                                description={
                                    <span>
                                        No leaderboards yet in this month
                                    </span>
                                }></Empty>
                        </Box>
                    ) : (
                        <>
                            <CloseButton
                                handleClose={() =>
                                    history.push(routes.startGame)
                                }
                                color="white"
                                className="leaderboards__board__close"
                            />
                            <div className="leaderboards__board__title">
                                LEADER BOARD
                            </div>
                            <div className="leaderboards__board__titles-row">
                                <span className="w-1">RANK</span>
                                <span className="w-2">NAME</span>
                                <span className="w-3">LEVEL</span>
                                <span className="w-4">SCORE</span>
                            </div>
                            <div className="leaderboards__board__data-rows">
                                {leaderboards.map((leaderboardScore, i) => (
                                    <div
                                        className={`leaderboards__board__data-rows__row ${
                                            IsCurrentUserScore(leaderboardScore)
                                                ? "current"
                                                : ""
                                        }`}>
                                        <span className="w-1">
                                            {DetermineRank(i + 1)}
                                        </span>
                                        <span className="w-2">
                                            {/* {IsCurrentUserScore(leaderboardScore) ? "YOU" : leaderboardScore.UserName} */}
                                            {leaderboardScore.UserName}
                                        </span>
                                        <span className="w-3">
                                            {DetermineGameLevel(
                                                leaderboardScore.GameLevel
                                            )}
                                        </span>
                                        <span className="w-4">
                                            {CommaNumber(
                                                leaderboardScore.Score
                                            )}
                                        </span>
                                    </div>
                                ))}
                                {leaderboardStore?.Rank?.rankPlace > 10 && (
                                    <div
                                        className={`leaderboards__board__data-rows__row current`}>
                                        <span className="w-1">
                                            {leaderboardStore.Rank.rankPlace}
                                        </span>
                                        <span className="w-2">
                                            {/* {IsCurrentUserScore(leaderboardScore) ? "YOU" : leaderboardScore.UserName} */}
                                            {
                                                leaderboardStore
                                                    .LeaderboardScore.UserName
                                            }
                                        </span>
                                        <span className="w-3">
                                            {DetermineGameLevel(
                                                gameStore.gameDifficulty
                                            )}
                                        </span>
                                        <span className="w-4">
                                            {CommaNumber(
                                                leaderboardStore.Rank.score
                                            )}
                                        </span>
                                    </div>
                                )}
                            </div>

                            <GameButton
                                color="primary"
                                text="START"
                                className="leaderboards__board__start-btn"
                                onClick={() => {
                                    history.push(routes.chooseLevel);
                                }}
                            />
                        </>
                    )}
                </div>
            )}
        </main>
    );
};

export default observer(LeaderboardsScreen);
