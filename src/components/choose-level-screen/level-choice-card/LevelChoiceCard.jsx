import React from "react";
import GameText from "../../generic/game-text/GameText";
import { GrTarget } from "react-icons/gr";
import GameButton from "../../generic/game-button/GameButton";
import { CommaNumber } from "./../../../utils/NumbersHelper";

import "./LevelChoiceCard.scss";

const LevelChoiceCard = (props) => {
    const {
        level /* (string) required */,
        days /* required */,
        jobsCount /* required */,
        budget /* required */,
        applicantsGoal /* required */,
        imgSrc /* required */,
        onClick /* required */,
        className /* optional */,
    } = props;

    return (
        <div className={`level-choice-card ${className && className}`}>
            <img
                src={imgSrc}
                alt="LevelPicture"
                className="level-choice-card__robot-img"
            />

            <div className="level-choice-card__top-section">
                <GameText
                    className="level-choice-card__level"
                    text={level}
                    fontSize={25}
                    lineHeight="30px"
                />
                <ul className="level-choice-card__ul">
                    <li className="level-choice-card__ul__li">
                        <GrTarget />
                        {days} Days
                    </li>
                    <li className="level-choice-card__ul__li">
                        <GrTarget />
                        {jobsCount} Jobs
                    </li>
                    <li className="level-choice-card__ul__li">
                        <GrTarget />${CommaNumber(budget)} BUDGET
                    </li>
                </ul>
            </div>

            <div className="level-choice-card__bottom-section">
                <GameText
                    text={`GOAL: ${CommaNumber(applicantsGoal)} APPLICANTS`}
                    fontSize={13}
                    lineHeight="29px"
                    className="level-choice-card__bottom-section__goal"
                />
                <GameButton
                    onClick={onClick}
                    color="primary"
                    text="START"
                    className="level-choice-card__bottom-section__btn"
                />
            </div>
        </div>
    );
};

export default LevelChoiceCard;
