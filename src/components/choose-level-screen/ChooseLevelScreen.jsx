import React, { useState, useContext, useEffect, useRef } from "react";

import { useHistory } from "react-router-dom";
import { routes } from "./../../constants/Routes";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "./../../stores/RootStoreContext";
import LevelChoiceCard from "./level-choice-card/LevelChoiceCard";
import { Box, Grid, TextField } from "@material-ui/core";
import SquareLoader from "./../generic/loader/Loader";
import GameText from "../generic/game-text/GameText";
import { GetImageSrcFromDifficulty } from "./ChooseLevelHelper";
import { AccountCircle } from "@material-ui/icons";

import "./ChooseLevelScreen.scss";
import { ParseLevelName } from './../../utils/GameHelpers';

const ChooseLevelScreen = () => {
    const { gameStore } = useContext(RootStoreContext);
    const [gameSetups, setGameSetups] = useState();
    // const userNameRef = useRef();
    // const [userName, setUserName] = useState("");
    const [userNameState, setUserNameState] = useState({
        value: "",
        isError: false,
    });
    const history = useHistory();

    /**
     * Functions
     */
    const HandleDifficultyClick = async (difficulty) => {
        if (!userNameState.value || userNameState.value == "") {
            setUserNameState({
                ...userNameState,
                isError: true,
            });
            return;
        }

        await gameStore.GenerateGameState(difficulty, userNameState.value);
        history.push(routes.play);
    };

    const RenderComponent = () => {
        return (
            <div className="choose-level-screen__wrapper">
                <GameText
                    text="CHOOSE NAME AND LEVEL"
                    fontSize={23}
                    fontHeight="28px"
                    className="choose-level-screen__wrapper__title"
                />

                <Grid
                    container
                    spacing={1}
                    alignItems="flex-end"
                    className="choose-level-screen__wrapper__name">
                    <Grid item>
                        <AccountCircle />
                    </Grid>
                    <Grid item>
                        <TextField
                            error={userNameState.isError}
                            helperText={userNameState.isError ? "Please fill in your username" : ""}
                            // ref={userNameRef}
                            value={userNameState.value}
                            onChange={(e) =>
                                setUserNameState({
                                    ...userNameState,
                                    value: e.target.value,
                                })
                            }
                            variant="filled"
                            id="username"
                            label="Username"
                        />
                    </Grid>
                </Grid>



                <div className="choose-level-screen__wrapper__choice-cards">
                    {[...gameSetups].reverse().map((gameSetup) => (
                        <LevelChoiceCard
                            key={gameSetup.campaign_name}
                            level={ParseLevelName(gameSetup.difficulty)}
                            days={gameSetup.days_duration}
                            jobsCount={gameSetup.jobs.length}
                            budget={gameSetup.given_budget}
                            applicantsGoal={gameSetup.applicants_goal}
                            imgSrc={GetImageSrcFromDifficulty(
                                gameSetup.difficulty
                            )}
                            onClick={async () => {
                                await HandleDifficultyClick(
                                    gameSetup.difficulty
                                );
                            }}
                            className={`card-${gameSetup.difficulty}`}
                        />
                    ))}
                </div>
            </div>
        );
    };

    useEffect(() => {
        (async () => {
            setGameSetups(await gameStore.gameSetups.GetGameSetups());
        })();
    }, []);

    return (
        <main className="choose-level-screen" style={{ paddingTop: "10vh" }}>
            {gameStore.gameSetups.isStateLoaded && gameSetups ? (
                RenderComponent()
            ) : (
                <Box mt={10}>
                    <SquareLoader />
                </Box>
            )}
        </main>
    );
};

export default observer(ChooseLevelScreen);
