import easyLogo from "../../assets/images/robot-easy.svg";
import mediumLogo from "../../assets/images/robot-medium.svg";
import HardLogo from "../../assets/images/robot-hard.svg";

export const GetImageSrcFromDifficulty = (difficulty) => {
    switch (difficulty) {
        case 1:
            return easyLogo;
        case 2:
            return mediumLogo;
        case 3:
            return HardLogo;
    }
};
