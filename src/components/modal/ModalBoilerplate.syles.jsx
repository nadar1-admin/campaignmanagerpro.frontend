import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        // background:
        //     "transparent linear-gradient(1deg, #84C2B9 0%, #84C2B9 17%, #248886 73%, #107678 100%) 0% 0% no-repeat padding-box",
        background:
            "transparent linear-gradient(1deg, rgba(132, 194, 185, 0.4) 0%, rgba(132, 194, 185, 0.4) 17%, rgba(36, 136, 134, 0.4) 73%, rgba(16, 118, 120, 0.4) 100%) 0% 0% no-repeat padding-box",
        border: "1px solid #707070",
    },
    paper: {
        boxSizing: 'border-box',
        background: "#38A999 0% 0% no-repeat padding-box",
        boxShadow: "inset 0px 1px 2px #FFFFFF, 3px 3px 6px #22567D",
        borderRadius: "8px",
        opacity: 0.91,
        backdropFilter: "blur(50px)",
        width: '800px',
        // minHeight: '540px',
        minHeight: '340px',
        outline: 'none',
        padding: 90
    },
}));
