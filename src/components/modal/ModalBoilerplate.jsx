import React, { useState, useEffect, useContext } from "react";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import CloseButton from "../generic/CloseButton/CloseButton";
import ModalHeaderStripe from "./ModalHeaderStripe/ModalHeaderStripe";

import { observer } from "mobx-react-lite";
import { useStyles } from "./ModalBoilerplate.syles";
import "./ModalBoilerplate.scss";

const ModalBoilerplate = (props) => {
    const {
        children,
        open,
        handleClose,
        headerStripeBackground,
        headerStripeText,
        headerStripeSrc /* Optional */,
        headerStripeLogoStyle /* Optional */,
        className /* Optional */,
    } = props;
    const classes = useStyles();

    return (
        <Modal
            className={classes.modal}
            open={open}
            disableScrollLock={true}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={open}>
                <div className={`game-modal ${classes.paper} ${className}`}>
                    <ModalHeaderStripe
                        text={headerStripeText}
                        background={headerStripeBackground}
                        imageSrc={headerStripeSrc}
                        style={headerStripeLogoStyle}
                    />
                    <CloseButton
                        color="primary"
                        handleClose={handleClose}
                        className="game-modal__close"
                    />
                    {children}
                </div>
            </Fade>
        </Modal>
    );
};
export default observer(ModalBoilerplate);
