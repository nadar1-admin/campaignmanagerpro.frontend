import React, { useState, useEffect, useContext } from "react";
import { RootStoreContext } from "../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";
import ModalBoilerplate from "../ModalBoilerplate";
import VolumeBar from "../VolumeBar/VolumeBar";
import GameButton from "../../generic/game-button/GameButton";
import { VscDebugRestart } from "react-icons/vsc";
import { AiFillHome } from "react-icons/ai";
import { FaPlay } from "react-icons/fa";
import { useHistory } from "react-router-dom";
import { routes } from "./../../../constants/Routes";
import { useSnackbar } from "notistack";
import useIsPlayScreen from "./../../../hooks/useIsGameplayScreen";
import { Box } from "@material-ui/core";

import "./SettingsModal.scss";

const SettingsModal = (props) => {
    const { gameStore } = useContext(RootStoreContext);
    const history = useHistory();
    const { enqueueSnackbar } = useSnackbar();

    const handleClose = () => {
        gameStore.settingsIsOpen = false;
    };

    const isPlayScreen = useIsPlayScreen();

    const HomeButton = () => (
        <GameButton
            onClick={() => {
                history.push(routes.startGame);
                gameStore.settingsIsOpen = false;
            }}
            className="settings-modal__buttons__button"
            color="secondary"
            hoverAnimation={false}>
            <AiFillHome />
        </GameButton>
    );

    return (
        <ModalBoilerplate
            open={gameStore.settingsIsOpen}
            handleClose={handleClose}
            headerStripeBackground="#38A999 0% 0% no-repeat padding-box"
            headerStripeText={"SETTINGS"}>
            {/* Body */}
            <div className="settings-modal">
                <VolumeBar />

                <div className="settings-modal__buttons">
                    {isPlayScreen ? (
                        <>
                            <HomeButton />

                            <GameButton
                                onClick={async () => {
                                    enqueueSnackbar("GAME RESTARTING");
                                    await gameStore.RestartGame();
                                    enqueueSnackbar("You started a new game", {
                                        variant: "success",
                                    });
                                }}
                                className="settings-modal__buttons__button restart"
                                color="secondary"
                                hoverAnimation={false}>
                                <VscDebugRestart />
                            </GameButton>

                            <GameButton
                                onClick={handleClose}
                                className="settings-modal__buttons__button play"
                                color="primary"
                                hoverAnimation={false}>
                                <FaPlay />
                            </GameButton>
                        </>
                    ) : (
                        <Box
                            display="flex"
                            justifyContent="space-around"
                            width="100%">
                            <HomeButton />

                            <GameButton
                                onClick={handleClose}
                                className="settings-modal__buttons__button play"
                                color="primary"
                                hoverAnimation={false}>
                                <FaPlay />
                            </GameButton>
                        </Box>
                    )}
                </div>
            </div>
        </ModalBoilerplate>
    );
};
export default observer(SettingsModal);
