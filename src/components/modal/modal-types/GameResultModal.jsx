import React, { useState, useEffect, useContext } from "react";
import ModalActionButton from "../ModalActionButton/ModalActionButton";
import { RootStoreContext } from "../../../stores/RootStoreContext";
import { observer } from "mobx-react-lite";
import { GrTarget } from "react-icons/gr";
import ModalBoilerplate from "../ModalBoilerplate";
import skullSvg from "../../../assets/images/skull-logo.svg";
import starSvg from "../../../assets/images/win-star-logo.svg";
import GameButton from "../../generic/game-button/GameButton";
import { Divider } from "@material-ui/core";
import { useSnackbar } from "notistack";
import { useHistory } from "react-router";

import "./GameResultModal.scss";
import { routes } from "./../../../constants/Routes";

const GameResultModal = (props) => {
    const [open, setOpen] = useState();
    const { gameStore, leaderboardStore } = useContext(RootStoreContext);
    const history = useHistory();
    const { enqueueSnackbar } = useSnackbar();

    const isWinner = gameStore.gameResult.HasReachedGoal;
    const score = gameStore.gameResult.Score;

    const handleClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        setOpen(gameStore.gameResult.IsGameFinished);
    }, [gameStore.gameResult.IsGameFinished]);

    return (
        <ModalBoilerplate
            open={open}
            handleClose={handleClose}
            headerStripeBackground={
                isWinner
                    ? "#63238C 0% 0% no-repeat padding-box"
                    : "#d0352e 0% 0% no-repeat padding-box"
            }
            headerStripeText={isWinner ? "YOU WIN" : "YOU LOSE"}
            headerStripeSrc={isWinner ? starSvg : skullSvg}
            headerStripeLogoStyle={
                isWinner
                    ? {
                          top: "-35px",
                      }
                    : {
                          left: "20px",
                          top: "-20px",
                      }
            }>
            {/* Body */}
            {!isWinner && (
                <div className="game-modal__explanation">
                    <span className="title">What... Why?</span>
                    <p>
                        You've spent all your budget before the campaign ended,
                        or didn't reach your applicant goal. Are you trying to
                        make us go broke?
                    </p>
                </div>
            )}

            {gameStore.gameResult.UserImprovementFeedback.length > 0 && (
                <div className="game-modal__improvement-feedback">
                    <span className="title">What can I do to improve? </span>
                    <ul>
                        {gameStore.gameResult.UserImprovementFeedback.map(
                            (feedback) => (
                                <li className="game-modal__improvement-feedback__item">
                                    <GrTarget />
                                    {feedback}
                                </li>
                            )
                        )}
                    </ul>
                </div>
            )}

            <div className="game-modal__stats">
                <span className="title">RESULTS</span>
                <div className="game-modal__stats__result-container">
                    <div>
                        <p className="game-modal__stats__title">TOTAL SCORE</p>
                        <p className="game-modal__stats__score">{score}</p>
                    </div>

                    <Divider
                        className="game-modal__stats__result-container__divider"
                        orientation="vertical"
                        flexItem
                    />
                    <div>
                        <p className="game-modal__stats__title">YOUR PLACE</p>
                        <p className="game-modal__stats__score">{leaderboardStore.Rank.rankPlace}</p>
                    </div>
                </div>
            </div>

            <div className="game-modal__buttons">
                <GameButton
                    onClick={async () => {
                        history.push(routes.leaderBoards);
                    }}
                    color="secondary"
                    text="LEADERS"
                    className="game-modal__buttons__button"
                />
                <GameButton
                    onClick={async () => {
                        enqueueSnackbar("GAME RESTARTING");
                        await gameStore.RestartGame();
                        enqueueSnackbar("You started a new game", {
                            variant: "success",
                        });
                    }}
                    className="game-modal__buttons__button"
                    text={isWinner ? "PLAY AGAIN" : "TRY AGAIN"}
                />
            </div>
        </ModalBoilerplate>
    );
};
export default observer(GameResultModal);
