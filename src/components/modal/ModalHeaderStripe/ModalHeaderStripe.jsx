import React from "react";

import "./ModalHeaderStripe.scss";

const ModalHeaderStripe = (props) => {
    const { text, background, imageSrc, style } = props;

    // const AddWinOrLoseClassName = () => {
    //     if (isWinner) return "win";
    //     return "lose";
    // };

    return (
        <div style={{ background }} className={`modal-header-stripe`}>
            <img
                style={style}
                className={`modal-header-stripe__svg`}
                src={imageSrc}
                alt=""
            />
            {text}
        </div>
    );
};

export default ModalHeaderStripe;
