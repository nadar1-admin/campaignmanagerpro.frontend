import React, { useState, useContext } from "react";
import { VolumeDown, VolumeUp } from "@material-ui/icons";
import { Box, Grid, Slider } from "@material-ui/core";
import { RootStoreContext } from "./../../../stores/RootStoreContext";

import "./VolumeBar.scss";
import { SetBgVolume } from "./../../../utils/SoundEffectUtil";
import { observer } from 'mobx-react-lite';

const VolumeBar = () => {
    const { gameStore } = useContext(RootStoreContext);

    const handleChange = (event, newValue) => {
        SetBgVolume(newValue);
        gameStore.settingsVolume = newValue;
    };

    return (
        <Box display="flex" alignItems="center" className="settings-volume-bar">
            <VolumeDown />

            <Box mx={2} width="100%">
                <Slider value={gameStore.settingsVolume} onChange={handleChange} />
            </Box>

            <VolumeUp />
        </Box>
    );
};

export default observer(VolumeBar);
