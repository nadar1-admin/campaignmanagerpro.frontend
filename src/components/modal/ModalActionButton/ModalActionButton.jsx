import React from 'react';
import './ModalActionButton.scss';

const ModalActionButton = (props) => {
    const {text} = props;
    return (
        <button className="modal-action-button">
            {text}
        </button>
    )
}

export default ModalActionButton
