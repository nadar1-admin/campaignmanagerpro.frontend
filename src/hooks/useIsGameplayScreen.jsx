import React from 'react'
import { useLocation } from 'react-router-dom';

const useIsPlayScreen = () => {
    const location = useLocation();
    const isPlayScreen = location.pathname == "/play";
    return isPlayScreen;
}

export default useIsPlayScreen
