import { observable, computed, action, makeObservable, toJS } from "mobx";
import { GetGameSetup, SimulateDay, GetVendors } from "../services/GameService";
import { v4 as uuidv4 } from "uuid";
import { ToFixedReturnNumber } from "../utils/NumbersHelper";
import { Sleep } from "./../utils/Sleep";
import { GameSetups } from "../models/game/GameSetups";
import { GamePerformance } from "../models/game/GamePerformance";
import { BidHandler } from "./handlers/BidHandler";
import { VendorOptions } from "./../models/game/VendorOptions";
import { GameResult } from "../models/game/GameResult";
import { LeaderboardStore } from "./LeaderboardStore";

export class GameStore {
    /**
     * CONSTANTS
     */
    ROUND_TIME = 60;

    /**
     * fields
     */
    #leaderboardStore;

    gameId = uuidv4();
    userName = undefined;
    gameState = undefined; // Game state is the jobs, goals, etc
    jobsData = undefined;
    spentBudget = 0;
    gameDifficulty = undefined;
    daysRemaining = undefined;
    showLoaderInGameScreen = false;
    // performanceByDays = [];
    // chartData = [];
    // vendorList = [];

    // gameResult = {
    //     isFinished: false,
    //     result: {
    //         hasUserWon: undefined,
    //         score: undefined,
    //     },
    // };

    roundTimeLeft = this.ROUND_TIME;

    /* Refactor to settings class */
    settingsIsOpen = false;
    settingsVolume = 40;

    /* Refactoring phase */
    gameResult = new GameResult(this);
    gameSetups = new GameSetups();
    gamePerformance = new GamePerformance(this);
    vendorOptions = new VendorOptions(this);

    /* Handlers */
    bidHandler = new BidHandler(this);

    /**
     *
     * @param {LeaderboardStore} leaderboardStore
     */
    constructor(leaderboardStore) {
        this.#leaderboardStore = leaderboardStore;

        makeObservable(this, {
            gameId: observable,
            userName: observable,
            gameState: observable,
            jobsData: observable,
            spentBudget: observable,
            gameDifficulty: observable,
            daysRemaining: observable,
            showLoaderInGameScreen: observable,
            // vendorList: observable,
            // performanceByDays: observable,
            // chartData: observable,
            // gameResult: observable,
            roundTimeLeft: observable,
            settingsIsOpen: observable,
            settingsVolume: observable,
            /* Refactoring to bridge pattern */
            gameSetups: observable,
            gamePerformance: observable,
            gameResult: observable,
            /*  */
            // GetVendorList: action,
            SetJobsData: action,
            SetGameDifficulty: action,
            SetShowLoaderInGameScreen: action,
            SetSpentBudget: action,
            SetRoundTimeLeft: action,
            GenerateGameState: action,
            SimulateDay: action,
            // GetPerformanceSummary: computed,
            GetBudgetRemaining: computed,
            GetDaysPassed: computed,
        });
    }

    /**
     * Setters
     */
    SetJobsData(jobsData) {
        this.jobsData = jobsData;
    }

    SetGameDifficulty(gameDifficulty) {
        this.gameDifficulty = gameDifficulty;
    }

    SetShowLoaderInGameScreen(booleanState) {
        this.showLoaderInGameScreen = booleanState;
    }

    SetSpentBudget(budget) {
        // this.spentBudget = this.spentBudget + priceToAdd;
        this.spentBudget = budget;
    }

    // SetGameResult(isFinished, hasUserWon, score) {
    //     this.gameResult = {
    //         isFinished,
    //         result: {
    //             hasUserWon,
    //             score,
    //         },
    //     };
    // }

    SetRoundTimeLeft(timeLeft) {
        this.roundTimeLeft = ToFixedReturnNumber(timeLeft);
    }

    /**
     * Methods
     */

    async GenerateGameState(difficulty, userName) {
        if (difficulty > 3 || difficulty < 1) {
            throw new Error(
                "Only difficulties for low, medium and high are supported"
            );
        }
        this.showLoaderInGameScreen = true;

        this.SetGameDifficulty(difficulty);
        this.userName = userName;

        this.gameState = await this.gameSetups.GetGameSetup(difficulty - 1);
        this.SetJobsData(this.gameState.jobs);

        this.daysRemaining = this.gameState.days_duration;
        this.spentBudget = 0;

        /**
         * Create chart data
         */
        // for (let index = 0; index < this.gameState.days_duration; index++) {
        //     this.chartData.push({
        //         name: `Day ${index + 1}`,
        //         applicantsGoal: this.gameState.applicants_goal,
        //         applicants: undefined,
        //     });
        // }

        this.gamePerformance.InitializePerformanceChart(
            this.gameState.applicants_goal,
            this.gameState.days_duration
        );

        this.showLoaderInGameScreen = false;

        /**
        "days_duration": 21,
        "given_budget": 50000,
        "applicants_goal": 20,
        "cpa_goal": 11,
        "difficulty": 1
         */
    }

    async SimulateDay() {
        this.SetShowLoaderInGameScreen(true);

        console.log(this.GetDaysPassed);

        const simulationResponse = await SimulateDay(
            this.gameId,
            this.userName,
            this.jobsData,
            this.GetDaysPassed,
            this.gameDifficulty,
            this.gameState.given_budget,
            this.gamePerformance.SimulationVariationId
        );

        this.daysRemaining = this.daysRemaining - 1;

        // Calculating aggregation for response
        let applicants = 0;
        let clicks = 0;
        let cost = 0;
        let age = -1;
        simulationResponse["user_performance"].forEach((jobResponse) => {
            applicants += jobResponse.applicants;
            clicks += jobResponse.clicks;
            cost += jobResponse.cost;
            if (age == -1) {
                age = jobResponse.Age;
            }
        });

        // this.performanceByDays.push({
        //     day: this.GetDaysPassed,
        //     applicants,
        //     clicks,
        //     cost,
        // });
        this.gamePerformance.AddDailyPerformance(
            simulationResponse,
            this.GetDaysPassed
        );

        /**
         * Update jobsData
         */

        const newJobsData = this.jobsData.map((jobData) => {
            const simulationResponseForJobData = simulationResponse[
                "user_performance"
            ].find((x) => x.job_id == jobData.job_id);
            return {
                ...jobData,
                ...simulationResponseForJobData,
            };
        });
        this.SetJobsData(newJobsData);

        /**
         * Update spent budget
         */
        this.SetSpentBudget(cost);

        /**
         * Update chart data
         */
        // this.chartData[this.GetDaysPassed - 1] = {
        //     ...this.chartData[this.GetDaysPassed - 1],
        //     applicants,
        //     clicks,
        //     cost,
        // };

        /**
         * Check if game ended (win or lose)
         */

        if (this.gameResult.CheckIfGameEnded()) {
            await this.gameResult.EndGame();
            await this.#leaderboardStore.SaveLeaderboardScore({
                GameId: this.gameId,
                GameLevel: this.gameDifficulty,
                HasReachedGoal: this.gameResult.HasReachedGoal,
                HasBeatenAlgo: this.gameResult.HasBeatenAlgo,
                Score: this.gameResult.Score,
                UserName: this.userName,
            });
            await this.gameResult.OpenEndGameModal();
        }

        this.SetShowLoaderInGameScreen(false);

        // if (this.GetBudgetRemaining < 0) {
        //     this.gameResult.EndGame(false, this.gamePerformance);
        //     return;
        // }

        // if (this.daysRemaining == 0) {
        //     if (
        //         this.gamePerformance.UserAggregatedPerformance.applicants >=
        //         this.gameState.applicants_goal
        //     ) {
        //         this.gameResult.EndGame(true, this.gamePerformance);
        //         return;
        //     }

        //     this.gameResult.EndGame(false, this.gamePerformance);
        //     return;
        // }
    }

    async RestartGame() {
        if (!this.gameState || this.gameDifficulty == undefined) {
            throw new Error("Can't restart the game since it didn't start");
        }

        this.showLoaderInGameScreen = true;

        this.gameId = uuidv4();
        this.gameState = undefined;
        this.jobsData = undefined;
        this.spentBudget = 0;
        this.daysRemaining = undefined;

        this.gamePerformance = new GamePerformance(this);
        this.vendorOptions = new VendorOptions(this);
        this.gameResult = new GameResult(this);

        //this.performanceByDays = [];
        //this.chartData = [];

        await Sleep(2000);
        await this.GenerateGameState(this.gameDifficulty, this.userName);

        this.settingsIsOpen = false;
        this.showLoaderInGameScreen = false;

        // this.gameResult = {
        //     isFinished: false,
        //     result: {
        //         hasUserWon: undefined,
        //         score: undefined,
        //     },
        // };
    }

    /**
     * Computed state
     */
    // get GetPerformanceSummary() {
    //     if (!this.jobsData || this.jobsData.length == 0) {
    //         return {
    //             jobsCount: 0,
    //             applicants: 0,
    //             clicks: 0,
    //             cost: 0,
    //             cvr: 0,
    //         };
    //     }

    //     let applicants = 0,
    //         clicks = 0,
    //         cost = 0;

    //     this.jobsData.forEach((jobData) => {
    //         applicants += jobData.applicants;
    //         clicks += jobData.clicks;
    //         cost += jobData.cost;
    //     });

    //     return {
    //         jobsCount: this.jobsData.length,
    //         applicants,
    //         clicks,
    //         cost,
    //         cvr: applicants == 0 ? 0 : clicks / applicants,
    //     };
    // }

    get GetBudgetRemaining() {
        return this.gameState.given_budget - this.spentBudget;
    }

    get GetDaysPassed() {
        return this.gameState.days_duration - this.daysRemaining;
    }
}
