import React, { createContext } from "react";
import { RootStore } from "./RootStore";

export const RootStoreContext = createContext({});


export const RootStoreProvider= ({
    children,
}) => {
    return (
        <RootStoreContext.Provider value={new RootStore()}>
            {children}
        </RootStoreContext.Provider>
    );
};
