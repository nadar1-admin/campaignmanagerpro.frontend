import { observable, computed, action, makeObservable, toJS } from "mobx";
import { LeaderboardService } from "./../services/LeaderboardService";

export class LeaderboardStore {
    /**
     * ctor
     */
    #leaderboardService = new LeaderboardService();

    /**
     * @property {{UserName: string, Id: number, CreateDate: Date, HasBeatenAlgo: boolean, Score: number, GameId: string, GameLevel: number, HasReachedGoal: boolean}}
     */
    LeaderboardScore = {};

    /**
     * @property {{Id: number, rankPlace: number, score: number, createdate: Date}}
     */
    Rank = {};

    constructor(parameters) {
        makeObservable(this, {
            GetMonthlyLeaderboards: action,
        });
    }

    async GetMonthlyLeaderboards() {
        const monthlyLeaderboards =
            await this.#leaderboardService.GetMonthlyLeaderboards();
        return monthlyLeaderboards;
    }

    /**
     *
     * @param {{GameId: string, GameLevel: number, UserName: string, Score: number, HasReachedGoal: boolean, HasBeatenAlgo: boolean}} createLeaderboardScoreRequest
     */
    async SaveLeaderboardScore(createLeaderboardScoreRequest) {
        const saveLeaderboardScoreResponse =
            await this.#leaderboardService.SaveLeaderboardScore(
                createLeaderboardScoreRequest
            );

        this.LeaderboardScore = saveLeaderboardScoreResponse.EntityCreated;
        this.Rank = saveLeaderboardScoreResponse.Rank;

        return saveLeaderboardScoreResponse;
    }
}
