import { ToFixedReturnNumber } from "../../utils/NumbersHelper";
import { RandomInRangeFloatResult } from "./../../utils/RandomHelper";

export class BidHandler {
    #gameStore;

    constructor(gameStore) {
        this.#gameStore = gameStore;
    }

    /**
     *
     * @param {number} bid
     */
    SetBidForAllJobs(bid) {
        const jobsDataAfterBidChange = this.#gameStore.jobsData.map(
            (rowDataEntry) => {
                return {
                    ...rowDataEntry,
                    current_bid: bid,
                    vendor_specifics: rowDataEntry.vendor_specifics.map(
                        (vendorSpecific) => ({
                            ...vendorSpecific,
                            current_bid: bid,
                        })
                    ),
                };
            }
        );

        this.#gameStore.SetJobsData(jobsDataAfterBidChange);
    }

    /**
     *
     * @param {number} bid
     * @param {(job: any) => boolean} filterCallback
     *
     * Sets the bid for job (all vendors included) that pass the filterCallback
     */
    SetBidForJobsAndAllVendorsOfJobWhere(bid, filterCallback) {
        const jobsDataAfterBidChange = this.#gameStore.jobsData.map(
            (rowDataEntry) => {
                if (filterCallback(rowDataEntry) == false) {
                    return rowDataEntry;
                }

                return {
                    ...rowDataEntry,
                    current_bid: bid,
                    vendor_specifics: rowDataEntry.vendor_specifics.map(
                        (vendorSpecific) => ({
                            ...vendorSpecific,
                            current_bid: bid,
                        })
                    ),
                };
            }
        );

        this.#gameStore.SetJobsData(jobsDataAfterBidChange);
    }

    /**
     *
     * @param {number} jobId
     * @param {number} bid
     * @returns {number} rowIndex
     */
    SetBidForAllVendorsInJob(jobId, bid) {
        let rowIndex = -1;

        const jobsDataAfterBidChange = this.#gameStore.jobsData.map(
            (rowDataEntry, i) => {
                if (rowDataEntry.job_id != jobId) return rowDataEntry;

                rowIndex = i;

                return {
                    ...rowDataEntry,
                    current_bid: bid,
                    vendor_specifics: rowDataEntry.vendor_specifics.map(
                        (vendorSpecific) => ({
                            ...vendorSpecific,
                            current_bid: bid,
                        })
                    ),
                };
            }
        );

        this.#gameStore.SetJobsData(jobsDataAfterBidChange);
        return rowIndex;
    }

    /**
     *
     * @param {number} jobId
     * @param {number} vendorId
     * @param {number} bid
     * @returns {number}
     */
    SetBidForVendor(jobId, vendorId, bid) {
        let rowIndex = -1;

        const jobsDataAfterBidChange = this.#gameStore.jobsData.map(
            (rowDataEntry, i) => {
                if (rowDataEntry.job_id != jobId) {
                    return rowDataEntry;
                }

                rowIndex = i;

                return {
                    ...rowDataEntry,
                    vendor_specifics: rowDataEntry.vendor_specifics.map(
                        (vendorSpecific) => {
                            if (vendorSpecific.vendor_id != vendorId) {
                                return vendorSpecific;
                            }

                            return {
                                ...vendorSpecific,
                                current_bid: bid,
                            };
                        }
                    ),
                };
            }
        );

        this.#gameStore.SetJobsData(jobsDataAfterBidChange);
        return rowIndex;
    }

    /**
     *
     * @param {number} vendorId
     * @param {number} bid
     * @returns {void}
     *
     * Sets the given bid for all jobs that are duoed with the given vendor id
     */
    SetBidForAllJobsWithVendor(vendorId, bid) {
        const jobsDataAfterBidChange = this.#gameStore.jobsData.map(
            (rowDataEntry) => {
                return {
                    ...rowDataEntry,
                    vendor_specifics: rowDataEntry.vendor_specifics.map(
                        (vendorSpecific) => {
                            if (vendorSpecific.vendor_id != vendorId) {
                                return vendorSpecific;
                            }

                            return {
                                ...vendorSpecific,
                                current_bid: bid,
                            };
                        }
                    ),
                };
            }
        );

        this.#gameStore.SetJobsData(jobsDataAfterBidChange);
    }

    SetRandomBidsForAll() {
        const bid = ToFixedReturnNumber(RandomInRangeFloatResult(0.5, 1.5), 2);

        this.SetBidForAllJobs(bid);
    }

    SetRandomBidsForVendorsWithNoBid() {}

    SetBidForAllJobsAssociatedWithVendor(vendorId, bid) {}
}
