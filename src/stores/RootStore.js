import { GameStore } from "./GameStore";
import { LeaderboardStore } from "./LeaderboardStore";

export class RootStore {
    constructor() {
        this.leaderboardStore = new LeaderboardStore()
        this.gameStore = new GameStore(this.leaderboardStore)
    }
}