import React, {useEffect} from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { routes } from "./constants/Routes";
import StartGameScreen from "./components/start-game-screen/StartGameScreen";
import ChooseLevelScreen from "./components/choose-level-screen/ChooseLevelScreen";
import LeaderboardsScreen from "./components/leaderboards-screen/LeaderboardsScreen";
import PlayGameScreen from "./components/play-game-screen/PlayGameScreen";
import HeaderContainer from "./components/play-game-screen/components/Header/HeaderContainer";
import SettingsModal from "./components/modal/modal-types/SettingsModal";

import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './assets/styles/global.scss';
import "./App.css";


function App() {

    return (
        <Router>
            <HeaderContainer />

            <SettingsModal />

            <main>
                <Switch>
                    <Route exact path={routes.startGame}>
                        <StartGameScreen />
                    </Route>

                    <Route exact path={routes.chooseLevel}>
                        <ChooseLevelScreen />
                    </Route>

                    <Route exact path={routes.leaderBoards}>
                        <LeaderboardsScreen />
                    </Route>

                    <Route exact path={routes.play}>
                        <PlayGameScreen />
                    </Route>
                </Switch>
            </main>
        </Router>
    );
}

export default App;
