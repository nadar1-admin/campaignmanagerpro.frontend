class Urls {
    prefix =
        process.env.REACT_APP_ENV == "prod"
            ? "https://cmanager-api.algoteam.net"
            : "http://localhost:8000";

    gameSetups = `${this.prefix}/game/game_setup`;
    vendors = `${this.prefix}/game/vendors`;
    userFeedback = `${this.prefix}/game/user_feedback_heuristic`;

    GetGameSetup(difficulty) {
        return `${this.prefix}/game/game_setup/${difficulty}`;
    }

    SimulateDay(simulationVariationId) {
        return `${this.prefix}/game/simulate_day?simulation_variation_id=${simulationVariationId}`;
    }

    leaderboards = {
        getMonthlyLeaderboards: `${this.prefix}/leaderboards`,
        saveLeaderboardScore: `${this.prefix}/leaderboards`,
    };
}

export const urls = new Urls();
