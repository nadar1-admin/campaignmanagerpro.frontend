export const routes = {
    startGame: "/",
    chooseLevel: "/choose-level",
    leaderBoards: "/leaderboards",
    play: "/play"
}