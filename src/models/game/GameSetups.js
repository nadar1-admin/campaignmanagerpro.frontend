import { observable, computed, action, makeObservable, toJS } from "mobx";
import { urls } from "./../../constants/Urls";
import axios from "axios";

/**
 * Schema:
 *      "campaign_name": string,
 *      "difficulty": number,
 *      "jobs": Job[],
        "days_duration": number,
        "given_budget": number,
        "applicants_goal": number,
        "cpa_goal": number
 */

export class GameSetups {
    /* Fields */
    _gameSetups = undefined;

    /* Props */
    isStateLoaded = false;

    /**
     * Ctor
     */
    constructor() {
        makeObservable(this, {
            _gameSetups: observable,
            isStateLoaded: observable,
            GetGameSetups: action,
        });
    }

    async GetGameSetup(index) {
        if (this._gameSetups == undefined) {
            this._gameSetups = await this.GetGameSetups();
        }

        return this._gameSetups[index];
    }

    async GetGameSetups() {
        if (this._gameSetups == undefined) {
            const gameSetupsResponse = await axios.get(urls.gameSetups);
            this._gameSetups = gameSetupsResponse.data;
            this.isStateLoaded = true;
        }
        return this._gameSetups;
    }
}
