import { observable, computed, action, makeObservable, toJS } from "mobx";
import { GetGameSetup, SimulateDay, GetVendors } from "../../services/GameService";


export class VendorOptions {
    /**
     * Fields
     */
    #gameStore;
    vendorList = []
    gameVendorSelectionOptions = {}

    constructor(gameStore) {
        this.#gameStore = gameStore;

        makeObservable(this, {
            vendorList: observable,
            gameVendorSelectionOptions: observable,
            GetVendorList: action,
            SetGameVendorSelectionOptions: action,
        });
    }


    SetGameVendorSelectionOptions(vendorSelectionOptions) {
        this.gameVendorSelectionOptions = {...vendorSelectionOptions};
    }

    async GetVendorList() {
        if (!this.vendorList || this.vendorList.length == 0) {
            this.vendorList = await GetVendors();
        }

        return this.vendorList;
    }

    
}
