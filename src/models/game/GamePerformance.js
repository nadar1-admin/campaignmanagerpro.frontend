import { observable, computed, action, makeObservable, toJS } from "mobx";
import { RandomInRangeIntegerResult } from './../../utils/RandomHelper';

export class GamePerformance {
    #gameStore;

    SimulationVariationId;
    UserPerformanceByDays = [];
    AlgoPerformanceByDays = [];

    ChartPerformance = [];
    ChartActiveSection = "Applicants";

    /**
     * Ctor
     */

    constructor(gameStore) {
        this.#gameStore = gameStore;
        this.SimulationVariationId = RandomInRangeIntegerResult(1, 3)

        makeObservable(this, {
            UserPerformanceByDays: observable,
            AlgoPerformanceByDays: observable,
            ChartActiveSection: observable,
            InitializePerformanceChart: action,
            UpdateDailyChartPerformance: action,
            SetChartActiveSection: action,
            UserAggregatedPerformance: computed,
        });
    }

    /* Actions */

    InitializePerformanceChart(applicantGoal, gameDaysDuration) {
        if (this.ChartPerformance.length > 0) {
            this.ChartPerformance = [];
        }

        // Day 0 = all nill
        this.ChartPerformance.push({
            day: `Day 0`,
            applicantsGoal: applicantGoal,
            userApplicants: 0,
            userCost: 0,
            userClicks: 0,
            algoApplicants: 0,
            algoCost: 0,
            algoClicks: 0,
        });

        for (let index = 0; index < gameDaysDuration; index++) {
            this.ChartPerformance.push({
                day: `Day ${index + 1}`,
                applicantsGoal: applicantGoal,
            });
        }
    }

    UpdateDailyChartPerformance(userPerformance, algoPerformance, day) {
        const index = day; /* - 1 */

        this.ChartPerformance[index] = {
            ...this.ChartPerformance[index],
            userApplicants: userPerformance.applicants,
            userCost: userPerformance.cost,
            userClicks: userPerformance.clicks,
            algoApplicants: algoPerformance.applicants,
            algoCost: algoPerformance.cost,
            algoClicks: algoPerformance.clicks,
        };
    }

    SetChartActiveSection(activeSection) {
        if (
            activeSection != "Applicants" &&
            activeSection != "Clicks" &&
            activeSection != "Cost"
        ) {
            throw new Error("Invalid section selected for chart");
        }

        this.ChartActiveSection = activeSection;
    }

    _AddUserDailyPerformance(userPerformance, day) {
        let applicants = 0;
        let clicks = 0;
        let cost = 0;

        userPerformance.forEach((jobResponse) => {
            applicants += jobResponse.applicants;
            clicks += jobResponse.clicks;
            cost += jobResponse.cost;
        });

        return {
            day: day,
            applicants,
            clicks,
            cost,
        };
    }

    _AddAlgoDailyPerformance(algoPerformance, day) {
        return {
            day: day,
            applicants: algoPerformance.applicants,
            clicks: algoPerformance.clicks,
            cost: algoPerformance.cost,
        };
    }

    AddDailyPerformance(simulationResponse, day) {
        const userPerformance = this._AddUserDailyPerformance(
            simulationResponse["user_performance"],
            day
        );
        const algoPerformance = this._AddAlgoDailyPerformance(
            simulationResponse["algo_performance"],
            day
        );

        this.UserPerformanceByDays.push(userPerformance);
        this.AlgoPerformanceByDays.push(algoPerformance);

        this.UpdateDailyChartPerformance(userPerformance, algoPerformance, day);
    }

    /* Computed */
    get UserAggregatedPerformance() {
        if (!this.#gameStore.jobsData || this.#gameStore.jobsData.length == 0) {
            return {
                jobsCount: 0,
                applicants: 0,
                clicks: 0,
                cost: 0,
                cvr: 0,
            };
        }

        let applicants = 0,
            clicks = 0,
            cost = 0;

        this.#gameStore.jobsData.forEach((jobData) => {
            applicants += jobData.applicants;
            clicks += jobData.clicks;
            cost += jobData.cost;
        });

        return {
            jobsCount: this.#gameStore.jobsData.length,
            applicants,
            clicks,
            cost,
            cvr: applicants == 0 ? 0 : clicks / applicants,
        };
    }
}
