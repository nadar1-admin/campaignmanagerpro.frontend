import { observable, computed, action, makeObservable, toJS } from "mobx";
import { GetUserImprovementFeedback } from "../../services/GameService";

export class GameResult {
    /**
     * Fields
     */
    #gameStore;

    /* Props */
    IsGameFinished = false;
    Score = undefined;
    HasReachedGoal = false;
    HasBeatenAlgo = false;
    UserImprovementFeedback = [];

    /**
     * Ctor
     */
    constructor(gameStore) {
        this.#gameStore = gameStore;

        makeObservable(this, {
            IsGameFinished: observable,
            Score: observable,
            HasReachedGoal: observable,
            UserImprovementFeedback: observable,
            EndGame: action,
            OpenEndGameModal: action,
        });
    }

    /**
     * Methods
     */

    CheckIfGameEnded() {
        return (
            this.#gameStore.GetBudgetRemaining <= 0 ||
            this.#gameStore.daysRemaining == 0
        );
    }

    CalculateScore(hasReachedGoal) {
        let multiplier = hasReachedGoal ? 5 : 2;
        return (
            this.#gameStore.gamePerformance.UserAggregatedPerformance
                .applicants * multiplier
        );
    }

    DetermineIfUserReachedGoal() {
        if (
            this.#gameStore.GetBudgetRemaining < 0 ||
            this.#gameStore.gamePerformance.UserAggregatedPerformance
                .applicants < this.#gameStore.gameState.applicants_goal
        ) {
            return false;
        } else {
            return true;
        }
    }

    DetermineIfUserBeatenAlgo() {
        if(!this.DetermineIfUserReachedGoal()) return false;

        const userDaysLength =
            this.#gameStore.gamePerformance.UserPerformanceByDays.length;
        const algoDaysLength =
            this.#gameStore.gamePerformance.AlgoPerformanceByDays.length;

        return (
            (this.#gameStore.gamePerformance?.UserPerformanceByDays[
                userDaysLength - 1
            ]?.applicants ?? 0) >
            (this.#gameStore.gamePerformance?.AlgoPerformanceByDays[
                algoDaysLength - 1
            ]?.applicants ?? 1)
        );
    }

    /**
     * Ends game and calculates the score
     * @param {boolean} hasReachedGoal
     * @param {GamePerformance} gamePerformance
     * @returns {number} game score
     */
    async EndGame() {
        this.HasReachedGoal = this.DetermineIfUserReachedGoal();
        this.HasBeatenAlgo = this.DetermineIfUserBeatenAlgo();

        this.Score = this.CalculateScore(this.HasReachedGoal);
        this.UserImprovementFeedback = await GetUserImprovementFeedback(
            this.#gameStore.gameId,
            this.#gameStore.gameDifficulty,
            this.#gameStore.gamePerformance.SimulationVariationId,
            this.#gameStore.gameState.given_budget
        );

        return this.CalculateScore();
    }

    /**
     * Sets the properties to open the modal
     */
    OpenEndGameModal() {
        this.IsGameFinished = true;
    }
}
