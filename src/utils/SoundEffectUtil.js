export const PlayBackgroundAudio = () => {
    const audio = document.querySelector("#bg-audio");
    if(audio.duration > 0 && !audio.paused) {
        console.log("already playing")
        return;
    }
    audio.play();
}

const PlayClickEffect = (elementId) => {
    const playAudioElement = document.querySelector(elementId);
    playAudioElement.play();
};

export const PlayClickSoundEffect = () => {
    PlayClickEffect("#click-audio");
};

export const PlayHoverSoundEffect = () => {
    PlayClickEffect("#hover-audio");
};

export const PlaySimulateActionSoundEffect = () => {
    PlayClickEffect("#simulate-action-click-audio");
};

export const SetBgVolume = (volume) => {
    const bgAudio = document.querySelector("#bg-audio");

    if (volume >= 1) {
        bgAudio.volume = volume / 100;
        return;
    }

    bgAudio.volume = volume;
};
