export const RandomInRangeFloatResult = (min, max) => {
    return Math.random() * (max - min + 1) + min;
}
export const RandomInRangeIntegerResult = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
}