export const CommaNumber = (num) => {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const ToFixedReturnNumber = (value, decimalPoints) => {
    return Number(value.toFixed(decimalPoints));
};

export const IsNumber = (string) => {
    return !isNaN(Number(string));
};
