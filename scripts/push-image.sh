#!/bin/sh


local_repo=campaign-manager-frontend:dev
remote_repo=public.ecr.aws/w5n1s4o8/campaign-manager-frontend

version=$1

if [[ $1 == "" ]]; then
    echo "ERROR: Please provide an argument specifying the image tag version"
    exit 1
fi

cd ..

aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 912189574234.dkr.ecr.us-east-1.amazonaws.com
# aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws/w5n1s4o8

docker build -t $local_repo .

docker tag $local_repo $remote_repo:$version

docker push $remote_repo:$version

