#!/bin/bash

bucket_name=campaign-manager-web-prod
#backup_bucket_name=algo-ppr-web-backup
cloudfront_distribution_id=E3V4G7H0ZI6FWI
url=cmanager-pro.algoteam.net

cd ..

echo running build
npm run build:prod

# echo backing up
# aws s3 cp s3://$bucket_name s3://$backup_bucket_name --recursive

echo deploying to s3
aws s3 cp build/ s3://$bucket_name --recursive

echo invalidating cloud formation
aws cloudfront create-invalidation --distribution-id $cloudfront_distribution_id --paths "/*"

echo deployed completed. Visit: $url